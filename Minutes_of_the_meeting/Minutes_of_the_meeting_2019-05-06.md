# Minutes of the meeting 2019/05/06

Nikolaj's "War story"
- not having a system that works for everybody regarding project management
	Worked for a company to help bakeries to order new components when something broke
	Their system was to take a piece of paper where the manager wrote down "that round thing near the green thing", which he then passed to Nikolaj with a "please fix", that didn't work for him.
	This caused Nikolaj to quit. There was no time to build better / more accessible systems because much time was wasted on finding which component was meant.
	They promised influence and possibility to construct a system but management just wanted to continue what they've always done.
	Reconsider employment when you don't feel comfortable with the management.
	Passing paper can work well in small organizations, but when bigger it can be too difficult.


1:
	When he left for this meeting he gave a task to the group so they can configure everything  for when he comes back. His answer was "why should I do that?". Even though Thais tried to explain many times what had to be done.
	Is he always giving out tasks?
		Yes. But that's because if he doesn't, there will be three people on one task.
	Is it okay that workload is uneven?
		Not always, it can be hard to distribute.
		It's not okay if they are not willing to improve the situation (attitude thing).
	Why is it important that everyone contributes?
		If someone with better knowledge finishes faster they inform the others on how to do it/how it's done.
	- The division of work needs to be fair.
		○ Communication & making agreements
	Do you have any concerns about the project because of it?
		Yes, not because of their lack of work but because when the exams-day hits they might not know what they should know and "I feel responsible".
	I sat down with him and helped him with the project-test and it worked.
		You need to go through it together
		When there was a problem and he said to them to ask Morten about it
			They just sat around and waited until Morten came and then said that he should ask Morten instead
	You need to have fun while working together 
	Let them have their little wins even though you could solve it faster
	Who defines the project?:
		The whole team?
			I this case they probably didn't 
	Difference between assigning and claiming tasks:
		Claiming is more free and feels better
		Assigning is "forcing someone" to work on it
	When helping people you need to go down to a basic level
		Visual help via Block diagram to avoid confusion
			Make it "point-able" so everybody is on the same page

How vague are your tasks?
	Quite vague but works well
	If they are handed out they should be specific to allow communication
		I Disagree because it may limit people to work like you
		It is useful to specify so everyone
	Morten gets asked to make more specific tasks
		Motivated people work completely fine with vague tasks
		Specific guides make you follow it
		
Didn't plan any structure in GitLab so an incompetent person is worse as they sabotage
	A competent person with no time will also "sabotage" your progress and not communicate
	
You will meet people who are not motivated or hard to work with
	If not colleagues then it's costumers
	That's why it's good to learn now where it's no harm.

Work experience:
	Worked at Grocery-shop where costumer interaction was necessary 
		Costumers were like "Why do I even come here"
			Employee is like "Stay away costumer"
	Worked in web-design
		You have to go surprisingly low to understand each other
		Pre-designs is good
			But some just change  the goal all the time
		When are you done?
			Contracts
				Even in contracts it can be difficult  to specify
	Nikolaj gave a price-estimate
		Costumer and he met later for improvements
			Whenever costumer wanted more stuff it would cost extra.
	"Expect failure & embrace failure"
		Do a minimum & sketches to test and evaluate your direction
		
Project vs Operations:
	- Project would be with allocated resources and time (Usually something you don't have a clear picture of but want to realize)
	- Operations is making day to day work (Go from a state that doesn't repeat  to something you can/will repeat)
	What you may end up working as is 50/50 Project/Operations and it evolves more and more towards Operations.
	Grocerer-work would be operations
	If people included in projects have to many operations it may interfere with it
	
Education as project:
	Yes it is a project every semester / whole study is a project
	A good goal would be to get a job
	If you talk about it as a project:
		Define scope
			Ex. What's your goal
		Define time
			Ex. How many hours spent
		Define recources
			Ex. what to spend on it
		Documents : 
			Notes
			Old projects
			Etc.
		Who's a stakeholder?
		How to make sure that it's fulfilled?

We will repeat this next week. 
For more information look at the weekly plan
	
