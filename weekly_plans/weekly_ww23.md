---
Week: 23
Content: Project part 2 phase 3
Material: See links in weekly plan
Initials: MON/NISI
---

# Week 23

This is the second week of phase 3 `make it useful for the user`

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
None from the teachers at this time.

### Learning goals
None from the teachers at this time.

## Deliverables
* Mandatory weeky meetings with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
    5. Any other business (AOB)

## Schedule

Monday

* 8:15 Introduction to the day, general Q/A session
    - Q&A list is on [hackmd.io](https://hackmd.io/29oDMYmEQ5i1hfsrL1D98g)

* 08:45? Mock exam round-up
    - this includes feedack from/to teachers, and from/to student

Tuesday

* 8:15 Introduction to the day, general Q/A session
    - Q&A list is on [hackmd.io](https://hackmd.io/29oDMYmEQ5i1hfsrL1D98g)

* 8:45? You work

    Group morning meeting

        Agenda: Same as Monday


    Remember to come ask questions if you have any.  

* 9:00 Teacher meetings

    Timeslot for you weekly 10 min. meeting with the teachers.

    Remember to book a time and have an agenda prepared. 

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19s-itt2-project/19S_ITT2_exercises.pdf) for details, if applicable.


## Comments