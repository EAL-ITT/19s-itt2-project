---
Week: 20
Content: Project part 2 phase 2
Material: See links in weekly plan
Initials: MON/NISI
---

# Week 20

This is the first week of phase 2 `consolidation`

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
None from the teachers at this time.

### Learning goals
None from the teachers at this time.

## Deliverables
* Mandatory weeky meetings with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
    5. Any other business (AOB)

## Schedule

Monday

* 8:15 Introduction to the day, general Q/A session 

    * Absence from EEG last Monday - why and what to do?

* 9:00? You work

    Group morning meeting
    
        You will meet in your groups every morning and decide on who does what. This is part of the project management, and is mandatory.
        Ordinary agenda:
        1. (5 min) Round the table: What did I do, and what did I finish?
        2. (5-10 min) Review of tasks: Are they still relevant? do we need to add new ones?
        3. (5 min) Round the table: Claim one task each.

    Remember to come ask questions if you have any.  

* 10:30 Project experience exchange
    
    See week 18 for details.

* 12:15 OLA21 2nd attempt presentations in network lab (B2.19)

    * Relevant students will be informed

Tuesday

* 8:15 Introduction to the day, general Q/A session

* 8:45? Course evaluation 

    All students evaluate the project course in the survey: 
    
    [survey A class](https://www.survey-xact.dk/LinkCollector?key=1P27MZZZ3K3N)

    [survey B class](https://www.survey-xact.dk/LinkCollector?key=9GARVVPGC61J)

* 9:00 Project part 2 - phase 1 student presentations

    Rundown of:
    * goals
    * deliverables
    * current status
    * demo

* 12:15 You work

    Group morning meeting

        Same agenda as monday
        
    Remember to come ask questions if you have any. 

Thursday

* 8:15 Introduction to the day, general Q/A session

* 8:30? You work

    Group morning meeting

        Same agenda as monday


    Remember to come ask questions if you have any.  

* 9:00 Teacher meetings

    Timeslot for you weekly 10 min. meeting with the teachers.

    Remember to book a time and have an agenda prepared. 

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19s-itt2-project/19S_ITT2_exercises.pdf) for details, if applicable.


## Comments

* We have three days this week.
