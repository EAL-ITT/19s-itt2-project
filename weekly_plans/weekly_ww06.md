---
Week: 06
Content: Project part 1 phase 1
Material: See links in weekly plan
Initials: MON/NISI
---

# Week 06 Continue work on minimal system

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* raspberry pi communicating with atmega, and exchging data over serial connection.

### Learning goals
* Code reuse
    * Level 1: The student is able to read and understand code written by others
    * Level 2: The student is able to use code written by others and make simple modifications
    * Level 3: The student is able to reuse code made by others and reuse it in a new context

* Documentating code
  * Level 1: The student can explain the purpose and give examples of code documentation
  * Level 2: The student is able to document simple code
  * Level 3: The student is able to do design documentation and other code relevant documentation


## Deliverable
* Mandatory weeky meetings with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)

* ATMega328 to Rasperry Pi serial communication over UART
* Raspberry pi configuration requirements fulfilled

## Schedule

Monday

* 8:15 Introdution to the day, general Q/A session

        Code of conduct, see exercise 1

* 10:00 KNM project plan presentation (10 min)

* 10:15 Exercise 2 and 3

* 12:30 Exercise 2 + 3, QA and status + Study trip by train?

* 14:00 presentations

Tuesday

* 8:15 Introdution to the day, general Q/A session

* 8:30 Group morning meeting

    See week05 for details

* 9:00 Teacher meetings

    Timeslot for you weekly meeting with the teachers.
    
    Remember to book a time and have an agenda prepared.

* 9:00 Python, raspberry and serial. See exercise 4    

* 12:30 Exercise 4, QA and status


## Hands-on time

* Exercise 1: Code of conduct
    
    1. (10 min) Read code of conduct
    2. (5 min) Write your opinion in bullet points
    3. (15 min) Discuss in group, agree on your opinions as a group
    4. (30 min) On class discussion and decisions

* Exercise 2: ATmega328 minimal system code

    see exercises for week 06

* Exercise 3: Connect ATmega328 and Raspberry Pi

    see exercises for week 06

* exercise 4: raspberry pi + python + serial

    see exercises for week 06

