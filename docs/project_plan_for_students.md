---
title: '19S ITT2 Project'
subtitle: 'Project plan'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Morten Bo Nielsen \<mon@ucl.dk\>']
main_author: 'Morten Bo Nielsen'
date: \today
email: 'mon@ucl.dk'
left-header: \today
right-header: Project plan
skip-toc: false
---



# Background

This is the semester project for ITT2 where we will combine different technologies to go from sensor to cloud. This project will cover the first part of the project, and will match topics that are taught in parallel classes.


# Purpose


The main goal is to have a system where sensor data is send to a cloud server, presented and collected, and data and control signal may be send back also. This is a leaning project and the primary objective is for the students to get a good understanding of the challenges involved in making such a system and to give them hands-on experience with the implementation also.

For simplicity, the goals stated wil evolve around the technical project goals. It must be read using the weekly plan as a complementary source for the *learning* oriented goals.


# Goals

The overall system that is going to be build looks as follows

![project_overview](project_overview.png)

Reading from the left to the right
* Analog input/output and digital input/output: These are sensor of different kinds that we are to implement into the system. Initially we will work with a temperature sensor.
* ATMega328: The embedded system to run the sensor software and to be the interface to the seralconnection to the raspberry pi.
* Raspberry Pi: Minimal linux system relevant programs to upload/download data from the ATMega and to/from the APIs.
* Juniper router: Router to protect your internal system from the untrusted networks, and to enable access to the Internet and the "cloud servers"
* Reverse proxy/API gateway: This is a server/router that collects and protects the API endpoints and webservers implemented by each group.
* webservers: There are one webserver per group. It will include the REST API implementation and the user interface website.


Project deliveries are:
* A system reading and writing data to and from the sensors/actuators
* A REST API exposing the values of the sensors and option of setting and applying the actuator values.
* A secure firewall enabling connections to the API.
* Docmentation of all parts of the solution
* Regular meeting with project stakeholders.
* Final evaluation

The project itself will be divided into 3 phases and gitlab will be used as the project management platform.


# Schedule

The project is divided into three phases from now to the easter holidays.

See the lecture plan for details.


# Organization

[Considerations about the organization of the project may includes
    • Identification of the steering committee
    • Identification of project manager(s)
    • Composition of the project group(s)
    • Identification of external resource persons or groups

For each of the identified groups and people, their tasks must be specified and their role in the project must be clear. This could be done in relation to the goals of the project.]

TBD: This section must be completed by the students.


# Budget and resources

No monetary resources are expected. In terms of manpower, only the people in the project group are expected to contrbute,


# Risk assessment

[The risks involved in the project is evaluated, and the major issues will be listed. The plan will include the actions taken as part of the project design to handle the risk or the actions planned should a given risk materialize.]

TBD: This section must be completed by the students based on the pre-mortem meeting


# Stakeholders

[An analysis of the stakeholder is conducted to establish who has an interest in the project. There will be multiple stakeholders with diverse interests in the project.

Possible stakeholders
    • Internal vs. external
    • Positive vs. negative
    • Active vs. passive
A strategy could be planned on how to handle each stakeholder and how to handle stakeholders different (and/or conflicting) interests and priorities.
This could also include actions designed to transform a person or a group into a (positive) stakeholder, or increase the value of the project for a given stakeholder.]

TBD: This section is to be completed by the students.
TBD: Please consider finding a relevant external stakeholder.

# Communication

[The stakeholders may have some requirements or requests as to what reports or other output is published or used. Some part of the project may be confidential.
Whenever there is a stakeholder, there is a need for communication. It will depend on the stakeholder and their role in the project, how and how often communication is needed.
In this section, reports, periodic emails, meetings, facebook groups and any other stakeholder communication will be described]

TBD: Students fill out this part based on risk assessment and stakeholders.

# Perspectives

This project will serve as a template for other similar project, ike the second one in ITT2 or in 3rd semester.

It can also serve as an example of the students abilities in their portfolio, when applying for jobs or internships.


# Evaluation

[Evaluation is about how to gauge is the project was successful. This includes both the process and the end result. Both of which may have consequences on future projects.
The will be some documentation needed at the end (or during) the project, e.g. external funding will require some sort of reporting after the project has finished. This must be described.]

TBD: Students fill out this one also

# References

[Include any relevant references to legal documents, literature, books, home pages or other material that is relevant for the reader.]

TBD: Students may add stuff here at their discretion
TBD: or just write "None at this time"
